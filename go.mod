module gitlab.com/isittrue/data-importer

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/jackc/pgx/v4 v4.10.1
	github.com/pkg/errors v0.9.1
	go.mongodb.org/mongo-driver v1.5.0
)

IsItTrue.io - Data Importer
===========================

Read the data from MongoDB and import the data into PostgreSQL.

```ditta

+------------+      +---------+      +----------+      +------------+
|            |      |         |      |          |      |            |
|  Crawlers  | ---> | MongoDB | ---> | Importer | ---> | PostgreSQL |
|            |      |         |      |          |      |            |
+------------+      +---------+      +----------+      +------------+


```
It's going to take care of:

- Validation

```ditta

+-----------+
|           | ---> | NO | ---> | Write error to MongoDB |
| Is valid? |
|           | ---> | YES | ---> | Write to PostgreSQL |
+-----------+

```

- Cleaner

It'll remove extra spaces and wrong characters and redundant data.

- Normalize

It'll transform and normalize the data to be uniform.

The data importer is in charge of all the writing in this PostgreSQL database. Any other app will write no data in this DB.

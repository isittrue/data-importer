package importer

import (
	"context"
	"fmt"
	"strings"
	"time"
)

// Validator is used for data validation
type Validator struct {
	DateFormat      string
	FalseValues     map[string]bool
	TrueValues      map[string]bool
	HalfTrueValues  map[string]bool
	MostFalseValues map[string]bool
	MostTrueValues  map[string]bool
}

// ValidatorFromConfig create a new Validator from given a Config
func ValidatorFromConfig(config Config) *Validator {

	slideToMap := func(s []string, m map[string]bool) {
		for _, v := range s {
			m[strings.ToLower(v)] = true
		}
	}

	validator := &Validator{
		DateFormat:      config.DateFormat,
		FalseValues:     make(map[string]bool),
		TrueValues:      make(map[string]bool),
		HalfTrueValues:  make(map[string]bool),
		MostFalseValues: make(map[string]bool),
		MostTrueValues:  make(map[string]bool),
	}

	slideToMap(config.RatingF, validator.FalseValues)
	slideToMap(config.RatingT, validator.TrueValues)
	slideToMap(config.RatingHT, validator.HalfTrueValues)
	slideToMap(config.RatingMF, validator.MostFalseValues)
	slideToMap(config.RatingMT, validator.MostTrueValues)

	return validator
}

// ValidateTransform is going to validate an article entry
// return two read only channels, one with the valid articles
// and another one with a stream of errors
//
// The format will be transformed/normalized to 2006-01-02
// The rating will be also be changed/mapped to one of the values used in postgres
func (validator *Validator) ValidateTransform(
	ctx context.Context,
	input <-chan *Article) (<-chan *Article, <-chan error) {
	output := make(chan *Article)
	errors := make(chan error)

	writeError := func(err error, errors chan error) {
		select {
		case <-ctx.Done():
		case errors <- err:
		}
	}

	setDate := func(art *Article) error {
		t, err := time.Parse(validator.DateFormat, art.PublishedDate)
		if err != nil {
			return err
		}
		art.PublishedDate = t.Format("2006-01-02")
		if art.ClaimDate == "" {
			return err
		}
		t, err = time.Parse(validator.DateFormat, art.ClaimDate)
		if err == nil {
			art.ClaimDate = t.Format("2006-01-02")
		}
		return err
	}

	setRating := func(art *Article) {
		r := strings.ToLower(art.Rating)
		switch true {
		case validator.FalseValues[r]:
			art.Rating = "F"
		case validator.TrueValues[r]:
			art.Rating = "T"
		case validator.HalfTrueValues[r]:
			art.Rating = "HT"
		case validator.MostFalseValues[r]:
			art.Rating = "MF"
		case validator.MostTrueValues[r]:
			art.Rating = "MT"
		default:
			art.Rating = "NA"
		}
	}

	go func() {
		defer close(errors)
		defer close(output)
	loop:
		for {
			select {
			case <-ctx.Done():
				return
			case art, ok := <-input:
				if !ok {
					return
				}
				errorsCount := 0
				if art.Identifier == "" {
					errorsCount++
					writeError(fmt.Errorf("Empty identifier detected: %+v", art), errors)
				}
				if art.Title == "" {
					errorsCount++
					writeError(fmt.Errorf("Empty title detected: %+v", art), errors)
				}
				if art.Claim == "" {
					errorsCount++
					writeError(fmt.Errorf("Empty claim detected: %+v", art), errors)
				}
				if art.Rating == "" {
					errorsCount++
					writeError(fmt.Errorf("Empty rating detected: %+v", art), errors)
				}
				err := setDate(art)
				if err != nil {
					errorsCount++
					writeError(err, errors)
				}

				if errorsCount > 0 {
					continue loop
				}

				setRating(art)

				select {
				case <-ctx.Done():
				case output <- art:
				}
			}
		}
	}()
	return output, errors
}

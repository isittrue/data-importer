package importer

import (
	"context"
	"sync"
	"testing"
)

func TestValidator(t *testing.T) {
	input := []*Article{
		{
			Identifier:    "01",
			URL:           "http://www.factcheck.com/01",
			Title:         "Article 01",
			Author:        "Pepe Franco",
			PublishedDate: "February 21, 2019",
			Claim:         "A kilo of feathers weighs more than a kilo of lead",
			Rating:        "False",
		},
		{
			Identifier:    "02",
			URL:           "http://www.factcheck.com/02",
			Title:         "Article 02",
			Author:        "James Tailor",
			PublishedDate: "December 12, 1999",
			Claim:         "My name is Gilberto Gomez of Mesquita",
			Rating:        "True",
		},
		{
			Identifier:    "", // Identifier Empty error
			URL:           "http://www.factcheck.com/03",
			Title:         "Article 03",
			Author:        "Pepe Franco",
			PublishedDate: "February 3, 2021",
			Claim:         "A kilo of feathers weighs more than a kilo of lead",
			Rating:        "Mostly-False",
		},
		{
			Identifier:    "04",
			URL:           "http://www.factcheck.com/04",
			Title:         "", // Title empty error
			Author:        "Pepe Franco",
			PublishedDate: "February 21, 2019",
			Claim:         "A kilo of feathers weighs more than a kilo of lead",
			Rating:        "Mostly-True",
		},
		{
			Identifier:    "05",
			URL:           "http://www.factcheck.com/05",
			Title:         "Article 05",
			Author:        "Pepe Franco",
			PublishedDate: "21 February 2019", // Bad date format error
			Claim:         "A kilo of feathers weighs more than a kilo of lead",
			Rating:        "Some other rating", // NA
		},
		{
			Identifier:    "06",
			URL:           "http://www.factcheck.com/06",
			Title:         "Article 06",
			Author:        "Pepe Franco",
			PublishedDate: "February 21, 2019",
			Claim:         "", // Empty claim error
			Rating:        "False",
		},
		{
			Identifier:    "07",
			URL:           "http://www.factcheck.com/07",
			Title:         "Article 07",
			Author:        "Pepe Franco",
			PublishedDate: "February 3, 2019",
			Claim:         "Something something maybe maybe true maybe false maybe...",
			ClaimDate:     "January 15, 2019",
			Rating:        "", // Empty rating error
		},
		{
			Identifier:    "08",
			URL:           "http://www.factcheck.com/08",
			Title:         "Article 08",
			Author:        "Pepe Franco",
			PublishedDate: "February 3, 2019",
			Claim:         "Something something maybe maybe true maybe false maybe...",
			ClaimDate:     "January 15, 2019",
			Rating:        "Half-True",
		},
		{
			Identifier:    "09",
			URL:           "http://www.factcheck.com/09",
			Title:         "Article 09",
			Author:        "Pepe Franco",
			PublishedDate: "February 3, 2019",
			Claim:         "Something something maybe maybe true maybe false maybe...",
			ClaimDate:     "January 15, 2019",
			Rating:        "Mostly-True",
		},
		{
			Identifier:    "10",
			URL:           "http://www.factcheck.com/10",
			Title:         "Article 10",
			Author:        "Pepe Franco",
			PublishedDate: "February 3, 2019",
			Claim:         "Something something maybe maybe true maybe false maybe...",
			ClaimDate:     "January 15, 2019",
			Rating:        "Mostly-True",
		},
		{
			Identifier:    "11",
			URL:           "http://www.factcheck.com/11",
			Title:         "Article 11",
			Author:        "Pepe Franco",
			PublishedDate: "February 3, 2019",
			Claim:         "Something something maybe maybe true maybe false maybe...",
			ClaimDate:     "January 15, 2019",
			Rating:        "I don't know",
		},
	}

	validator := &Validator{
		DateFormat:      "January 2, 2006",
		FalseValues:     map[string]bool{"False": true},
		TrueValues:      map[string]bool{"True": true},
		HalfTrueValues:  map[string]bool{"Half-True": true},
		MostFalseValues: map[string]bool{"Mostly-False": true},
		MostTrueValues:  map[string]bool{"Mostly-True": true},
	}

	articles := make(chan *Article)

	go func() {
		defer close(articles)
		for _, art := range input {
			articles <- art
		}
	}()

	output, errors := validator.ValidateTransform(context.TODO(), articles)

	outputTotal := 0
	errorsTotal := 0

	var wg sync.WaitGroup

	wg.Add(1)

	go func() {
		defer wg.Done()
		for range errors {
			errorsTotal++
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for range output {
			outputTotal++
		}
	}()

	wg.Wait()

	if outputTotal != 6 {
		t.Errorf("Output total != 6: %d", outputTotal)
	}

	if errorsTotal != 5 {
		t.Errorf("Errors total != 5: %d", errorsTotal)
	}
}

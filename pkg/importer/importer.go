package importer

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Config is the importer configuration for a website
type Config struct {
	URL        string   `toml:"url"`
	DateFormat string   `toml:"date_format"`
	RatingF    []string `toml:"rating_false"`
	RatingT    []string `toml:"rating_true"`
	RatingHT   []string `toml:"rating_half"`
	RatingMF   []string `toml:"rating_mfalse"`
	RatingMT   []string `toml:"rating_mtrue"`
}

// Article represents an article document in MongoDB
type Article struct {
	Identifier    string   `json:"identifier"`
	URL           string   `json:"url"`
	Title         string   `json:"title"`
	Author        string   `json:"author"`
	PublishedDate string   `json:"publishedDate"`
	Claim         string   `json:"claim"`
	ClaimDate     string   `json:"claimDate"`
	Rating        string   `json:"rating"`
	Tags          []string `json:"tags"`
	Sources       []string `json:"sources"`
	Metadata      []byte
}

// Importer read the data from MongoDB
// Validate and transform that and import the results into PostgreSQL
// Write the errors to MongoDB
type Importer struct {
	name   string
	mgDB   *mongo.Database
	pgDB   *pgxpool.Pool
	config Config
}

// New create a new importer
// The name should be the same as the collection in mongo
func New(name string, mdb *mongo.Database, pgdb *pgxpool.Pool, cf Config) *Importer {
	return &Importer{
		name:   name,
		mgDB:   mdb,
		pgDB:   pgdb,
		config: cf,
	}
}

func (imp *Importer) getInput(ctx context.Context) <-chan *Article {
	input := make(chan *Article)
	go func() {
		defer close(input)
		collection := imp.mgDB.Collection(imp.name)
		cur, err := collection.Find(ctx, bson.D{})
		if err != nil {
			log.Printf("IMPORTER [%s]: error found => %+v",
				imp.name, errors.Wrap(err, "Importer Error: Collection"))
			return
		}
		defer cur.Close(ctx)
	loop:
		for cur.Next(ctx) {
			article := &Article{}
			err := cur.Decode(article)
			if err != nil {
				log.Printf("IMPORTER [%s]: error found => %+v",
					imp.name, errors.Wrap(err, "Importer Error: Decode"))
				continue loop
			}
			article.Metadata, err = json.Marshal(article)
			if err != nil {
				log.Printf("Unable to store metadata for article: %+v", article)
			}
			select {
			case <-ctx.Done():
			case input <- article:
			}
		}
		if err := cur.Err(); err != nil {
			log.Printf("IMPORTER [%s]: error found => %+v",
				imp.name, errors.Wrap(err, "Importer Error: Cursor"))
		}
	}()
	return input
}

// Import start the importer
func (imp *Importer) Import(ctx context.Context) {
	wid, err := getCreateWebsite(ctx, imp.pgDB, imp.name, imp.config.URL)
	if err != nil {
		log.Printf("IMPORTER [%s]: error found => %+v",
			imp.name, errors.Wrap(err, "Importer Error: Website"))
		return
	}

	validator := ValidatorFromConfig(imp.config)

	input := imp.getInput(ctx)
	output, errorsFound := validator.ValidateTransform(ctx, input)

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()

		maxRunning := 20
		c := maxRunning

		var ww sync.WaitGroup

		for {
			select {
			case <-ctx.Done():
				return
			case article, ok := <-output:
				if !ok {
					goto done
				}
				ww.Add(1)
				go func() {
					defer ww.Done()
					aid, err := addUpdateArticle(ctx, imp.pgDB, article, wid)
					if err != nil {
						log.Printf("IMPORTER [%s]: error found => %+v",
							imp.name, errors.Wrap(err, "Importer Error: Article"))
						return
					}
				tags:
					for _, tname := range article.Tags {
						tid, err := getCreateTag(ctx, imp.pgDB, tname)
						if err != nil {
							log.Printf("IMPORTER [%s]: error found => %+v (tag: %s)",
								imp.name, errors.Wrap(err, "Importer Error: Tags (Create)"), tname)
							continue tags
						}
						_, err = addTag(ctx, imp.pgDB, aid, tid)
						if err != nil {
							log.Printf("IMPORTER [%s]: error found => %+v",
								imp.name, errors.Wrap(err, "Importer Error: Tags (Add)"))
							continue tags
						}
					}
				}()

				c--
				if c <= 0 {
					c = maxRunning
					ww.Wait()
				}
			}
		}
	done:
		ww.Wait()
		log.Printf("IMPORTER[%s]: done", imp.name)
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		errorCollection := imp.mgDB.Collection(imp.name + "_errors")
		for {
			select {
			case <-ctx.Done():
				return
			case errFound, ok := <-errorsFound:
				if !ok {
					return
				}
				_, err := errorCollection.InsertOne(ctx, bson.D{{Key: "description", Value: errFound.Error()}})
				if err != nil {
					log.Printf("IMPORTER [%s]: error found => %+v",
						imp.name, errors.Wrap(err, "Importer Error: Errors"))
				}
			}
		}
	}()

	wg.Wait()
}

func getCreateWebsite(ctx context.Context, pgDB *pgxpool.Pool, name string, url string) (wid uint, err error) {
	err = pgDB.QueryRow(ctx, "SELECT id FROM websites WHERE name = $1 LIMIT 1", name).Scan(&wid)
	if err == pgx.ErrNoRows {
		err = pgDB.QueryRow(ctx,
			"INSERT INTO websites(name, url) VALUES($1, $2) RETURNING id",
			name, url).Scan(&wid)
	}
	return wid, err
}

func getCreateTag(ctx context.Context, pgDB *pgxpool.Pool, name string) (tid uint, err error) {
	name = strings.TrimSpace(name)
	err = pgDB.QueryRow(ctx, "SELECT id FROM tags WHERE name = $1 LIMIT 1", name).Scan(&tid)
	if err == pgx.ErrNoRows {
		err = pgDB.QueryRow(ctx,
			"INSERT INTO tags(name) VALUES($1) RETURNING id", name).Scan(&tid)
	}
	return tid, err
}

func addTag(ctx context.Context, pgDB *pgxpool.Pool, aid uint, tid uint) (rid uint, err error) {
	err = pgDB.QueryRow(ctx,
		"SELECT id FROM articles_tags WHERE article_id = $1 AND tag_id = $2 LIMIT 1",
		aid, tid).Scan(&rid)
	if err == pgx.ErrNoRows {
		err = pgDB.QueryRow(ctx,
			"INSERT INTO articles_tags(article_id, tag_id) VALUES($1, $2) RETURNING id",
			aid, tid).Scan(&rid)
	}
	return rid, err
}

func buildQueryForArticle(art *Article, wid uint) ([]string, []interface{}, []string) {
	publishedDate, _ := time.Parse("2006-01-02", art.PublishedDate)
	claimDate, _ := time.Parse("2006-01-02", art.ClaimDate)

	columns := []string{
		"website_id", "identifier", "url", "title", "author",
		"published_date", "claim", "rating", "metadata"}
	values := []interface{}{
		wid,
		strings.TrimSpace(art.Identifier),
		strings.TrimSpace(art.URL),
		strings.TrimSpace(art.Title),
		strings.TrimSpace(art.Author),
		publishedDate,
		strings.TrimSpace(art.Claim),
		art.Rating,
		art.Metadata}

	if !claimDate.IsZero() {
		columns = append(columns, "claim_date")
		values = append(values, claimDate)
	}

	if len(art.Sources) > 0 {
		sources := make([]string, 0)
		for _, s := range art.Sources {
			sources = append(sources, strings.TrimSpace(s))
		}
		columns = append(columns, "sources")
		values = append(values, sources)
	}

	refs := make([]string, 0)
	for rix := range columns {
		refs = append(refs, fmt.Sprintf("$%d", rix+1))
	}
	return columns, values, refs
}

func addUpdateArticle(ctx context.Context, pgDB *pgxpool.Pool, art *Article, wid uint) (aid uint, err error) {
	err = pgDB.QueryRow(ctx,
		"SELECT id FROM articles WHERE website_id = $1 AND identifier = $2 LIMIT 1",
		wid, art.Identifier).Scan(&aid)
	if err == nil {
		// UPDATE
		columns, values, _ := buildQueryForArticle(art, wid)
		update := make([]string, 0)
		for cix := range columns {
			update = append(update, fmt.Sprintf("%s = $%d", columns[cix], cix+1))
		}
		query := fmt.Sprintf("UPDATE articles SET %s WHERE id = %d", strings.Join(update, ", "), aid)
		_, err = pgDB.Exec(ctx, query, values...)
	} else if err == pgx.ErrNoRows {
		// INSERT
		columns, values, refs := buildQueryForArticle(art, wid)
		query := fmt.Sprintf("INSERT INTO articles(%s) VALUES(%s) RETURNING id",
			strings.Join(columns, ","), strings.Join(refs, ","))
		err = pgDB.QueryRow(ctx, query, values...).Scan(&aid)
	}
	return aid, err
}

/*
IsItTrue.io

Fact-checks Articles Data
*/

-- Websites table
CREATE TABLE websites (
    id serial PRIMARY KEY,
    url varchar(1024) NOT NULL UNIQUE,
    name varchar(1024) NOT NULL UNIQUE);

-- Articles table
CREATE TABLE articles (
    id serial PRIMARY KEY,
    website_id integer NOT NULL REFERENCES websites(id) ON DELETE CASCADE,
    identifier varchar(1024) NOT NULL,
    url varchar(2048) NOT NULL,
    title varchar(1024) NOT NULL,
    author varchar(256),
    published_date date NOT NULL,
    claim varchar(2048) NOT NULL,
    claim_date date,
    rating varchar(2) NOT NULL CHECK (rating IN ('T', 'F', 'MT', 'MF', 'HT', 'NA')),
    sources varchar(2048)[],
    metadata jsonb,
    UNIQUE(website_id, identifier));

-- Article indexes
CREATE INDEX articles_website_id_ix ON articles(website_id);
CREATE INDEX articles_identifier_ix ON articles(identifier);

-- Article text searches
CREATE INDEX articles_title_ix ON articles(lower(title) text_pattern_ops);
CREATE INDEX articles_claim_ix ON articles(lower(claim) text_pattern_ops);

-- Full searches
CREATE INDEX articles_claim_search_ix ON articles USING GIN (to_tsvector('english', claim));

-- Tags table
CREATE TABLE tags (
    id serial PRIMARY KEY,
    name varchar(512) NOT NULL UNIQUE);

-- Articles/Tags relationship
CREATE TABLE articles_tags (
    id bigserial PRIMARY KEY,
    article_id integer NOT NULL REFERENCES articles(id) ON DELETE CASCADE,
    tag_id integer NOT NULL REFERENCES tags(id) ON DELETE CASCADE);

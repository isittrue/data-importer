package main

import (
	"context"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/BurntSushi/toml"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/isittrue/data-importer/pkg/importer"
	mongoM "gitlab.com/isittrue/data-importer/pkg/mongo"
)

func getDatabaseConfig() (config map[string]map[string]string) {
	cfile, err := ioutil.ReadFile("database.toml")
	if err != nil {
		log.Fatal(err)
	}
	toml.Unmarshal(cfile, &config)
	return config
}

func getImporterConfig() (config map[string]importer.Config) {
	cfile, err := ioutil.ReadFile("importer.toml")
	if err != nil {
		log.Fatal(err)
	}
	toml.Unmarshal(cfile, &config)
	return config
}

func main() {
	configImporter := getImporterConfig()
	configDatabase := getDatabaseConfig()

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	mongoCli, err := mongoM.Connect(ctx, configDatabase["mongo"]["uri"])
	if err != nil {
		log.Fatal(err)
	}
	defer mongoM.Disconnect(ctx, mongoCli)

	mdb := mongoCli.Database(configDatabase["mongo"]["database"])

	pgdb, err := pgxpool.Connect(context.Background(), configDatabase["postgres"]["uri"])
	defer pgdb.Close()

	var wg sync.WaitGroup

	for website := range configImporter {
		importer := importer.New(website, mdb, pgdb, configImporter[website])
		wg.Add(1)
		go func() {
			defer wg.Done()
			importer.Import(ctx)
		}()
	}

	wg.Wait()
}
